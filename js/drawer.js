var isDrawing = false,
	oldX = 0,
	oldY = 0,
	brushSize = 4,
	penColor = "#000000",
	isErase = false,
	drawButton = document.getElementById("draw"),
	eraseButton = document.getElementById("erase");

function canDraw() {
	isErase = false;
}

function canErase() {
	isErase = true;
}

/*マウスの動きをよみとる
  もしかするとtouchendとかじゃないとだめかも、
  実機で動かなかったときは要検討*/
window.addEventListener("load", function() {
	var can = document.getElementById("myCanvas");
	can.addEventListener("mousemove", draw, true);
	can.addEventListener("mousedown", function(e) {
		isDrawing = true;
		oldX = e.clientX - $('canvas').offset().left;
		oldY = e.clientY - $('canvas').offset().top;
	}, true);
	can.addEventListener("mouseup", function() {
		isDrawing = false;
	}, true);

	$('li').click(function() {
		penColor = $(this).css('background-color');
	});
}, true);

//線を描画するやつ
function draw(e) {
	var offset = 5;
	var x = e.clientX - $('canvas').offset().left;
	var y = e.clientY - $('canvas').offset().top;
	var can = document.getElementById("myCanvas");
	var context = can.getContext("2d");
	if (!isDrawing) return;
	if (isErase) {
		context.globalCompositeOperation = 'destination-out'; //消しゴムのとき
	} else {
		context.globalCompositeOperation = 'source-over'; //ペンのとき
	}
	context.strokeStyle = penColor;
	context.lineWidth = brushSize;
	context.lineJoin = "round";
	context.lineCap = "round";
	context.beginPath();
	context.moveTo(oldX, oldY);
	context.lineTo(x, y);
	context.stroke();
	context.closePath();
	oldX = x;
	oldY = y;
}

//画像保存、canvas2Imageを利用して
function saveDate() {
	var can = document.getElementById("myCanvas");
	Canvas2Image.saveAsPNG(can);
}

//canvasの中身を一掃する
function clearCanvas() {
	var can = document.getElementById("myCanvas");
	var context = can.getContext("2d");
	context.clearRect(0, 0, $('canvas').width(), $('canvas').height());
}